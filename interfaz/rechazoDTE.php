<html>
<head>
<title> - RECHAZO DTE -</title>
<script languaje="javascript" type="text/javascript">

function LoadFormulario()
{
  var frm = window.document;
  
  var infoAccion = frm.getElementById("accion").value;
  
  if(infoAccion == "1")
  {
     frm.getElementById("tdEstadoDocumento").style.display = "";
  }
  else
  {
    frm.getElementById("tdEstadoDocumento").style.display = "none";  
  }
    
}

function TipoAccion(obj)
{
  var frm = window.document;
  if(obj.value == "1")
  {
     frm.getElementById("tdEstadoDocumento").style.display = "";
     frm.getElementById("estadoDocumento").focus(); 
  }
  else
  {
    frm.getElementById("tdEstadoDocumento").style.display = "none";  
  }  
}

function Valida()
{
  var frm = window.document;
  
  var accion = frm.getElementById("accion").value;
  var rutEmisor  = frm.getElementById("rutEmisor").value;
  var dvEmisor  = frm.getElementById("dvEmisor").value;
  var tipoDocumento  = frm.getElementById("tipoDocumento").value;
  var folio  = frm.getElementById("folio").value;
  
  if(accion == "0")
  {
    alert('Debe seleccionar accion');
    frm.getElementById("accion").focus();
    return false;   
  } 
  
  if(rutEmisor == "")
  {
    alert('Debe Ingresar Rut');
    frm.getElementById("rutEmisor").focus();
    return false;   
  } 
  
  if(dvEmisor == "")
  {
    alert('Debe Ingresar Digito Verificador');
    frm.getElementById("dvEmisor").focus();
    return false;   
  }
  
  if(tipoDocumento == "0")
  {
    alert('Debe Seleccionar Tipo de Documento');
    frm.getElementById("tipoDocumento").focus();
    return false;   
  }

  if(folio == "")
  {
    alert('Debe Ingresar Folio de Documento');
    frm.getElementById("folio").focus();
    return false;   
  }
    
    return true;

}

</script>
</head>
<body onLoad="LoadFormulario();">
<center>
<h1>RECHAZO DTE</h1>
<form action="../logica/rechazoDTE.php" method="POST">
<table>
<tr>
<td>Accion</td>
<td>
<select name="accion" id="accion" onchange="TipoAccion(this);">
<option value="0">- Seleccionar -</option>
<option value="1">Ingresar Aceptacion/Reclamo Doc.</option>
<option value="2">Listar Eventos Historico Doc.</option>
<option value="3">Consultar DocDte Cedible</option>
<option value="4">Consultar Fecha Recepcion Sii</option>
</select>
</td>
</tr>

<tr id="tdEstadoDocumento">
<td>Estado Documento</td>
<td>
<select name="estadoDocumento" id="estadoDocumento">
<option value="0">- Seleccionar -</option>
<option value="ACD">Acepta Contenido del Documento</option>
<option value="RCD">Reclamo al Contenido del Documento</option>
<option value="ERM">Otorga Recibo de Mercaderias o Servicios</option>
<option value="RFP">Reclamo por Falta Parcial de Mercaderias</option>
<option value="RFT">Reclamo por Falta Total de Mercaderias</option>
</select>
</td>
</tr>

<tr>
<td>Rut Emisor</td><td><input type="text" id="rutEmisor" name="rutEmisor" size="40" maxlength="10" placeholder="sin puntos"></td>
</tr>
<tr>
<td>DV Emisor</td><td><input type="text" id="dvEmisor" name="dvEmisor" size="2" maxlength="1" ></td>
</tr>

<tr>
<td>Tipo de Documento</td>
<td>
<select name="tipoDocumento" id="tipoDocumento">
<option value="0">- Seleccionar -</option>
<option value="33">Factura Electronica</option>
<option value="34">Factura Electronica Exenta</option>
<option value="43">Liquidacion Factura Electronica</option>
<option value="56">Nota de Debito Electronica</option>
<option value="61">Nota de Credito Electronica</option>
</select>
</td>
</tr>

<tr>

<td>Folio Documento</td><td><input type="text" id="folio" name="folio" size="10" ></td>

</tr>



<tr>

<td>&nbsp;</td><td><input type="submit" value="Enviar" onClick="return Valida();" ></td>

</tr>
</table>
</form>

</center>
<br>
<div>
<i>
Codigos de Respuesta a Observaciones a Considerar : 
<br>
<ul>
<li>‐1: Error Interno. Rut Receptor del Documento debe reintentar enviar la transaccion mas tarde.</li>
<li>1: Rut Emisor Erroneo.</li>
<li>2: Numero de Folio Erroneo.</li>
<li>3: Tipo de documento no corresponde.</li>
<li>4: Accion invalida.</li>
<li>5: DTE ya esta reclamado por XXX); Donde XXX, es el tipo de evento de reclamo que se haya registrado (RFP, RFT, RCD)</li>
<li>6: No se puede acusar recibo de mercaderia de DTE previamente reclamado por XXX); Donde XXX, es el tipo de evento de reclamo que se haya registrado (RFP, RFT, RCD)</li>
<li>7: Evento registrado previamente.</li>
<li>8: Pasados 8 días despues de la recepcion no es posible registrar reclamos o eventos.</li>
<li>9: No existen registros de acuerdo a los parametros ingresados.</li>
<li>10: Documento no emitido y/o recibido en el SII desde el 14 de Enero de 2017 en adelante.</li>
<li>11: No se puede reclamar DTE previamente aceptado.</li>
<li>12: No se puede dar por aceptado DTE previamente rechazado por XXX); Donde XXX, es el tipo de evento de reclamo que se haya registrado (RFP, RFT, RCD)</li>
<li>13: No se puede reclamar DTE previamente registrado como que acuso recibo mercaderia.</li>
<li>14: Accion autorizada solo para empresa receptora o emisora.</li>
<li>15: Listado de eventos del documento.</li>
<li>16: Documento no presenta eventos de reclamos o acuse de recibo.</li>
<li>17: Acción autorizada solo para empresa receptora.</li>
<li>18: Documento no ha sido recibido.</li>
<li>19: Reclamo de mercaderías ya realizado.</li>
<li>20: Tipo de documento no es cedible.</li>
<li>21: DTE No cedible, referenciado por nota de credito de anulacion del emisor dentro de los primeros 8 dias</li>
<li>22: No existe registro de reclamo o de recepcion de mercadería o servicios.</li>
<li>23: DTE Cedible, sin reclamos</li>
<li>24: DTE No Cedible, DTE reclamado por el receptor</li>
<li>25: DTE Cedible, habiendo pasado 8 días se entiende dado acuse de recibo</li>
</ul>
</i>
</div>
</body>

</html>