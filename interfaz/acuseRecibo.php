<html>
<head>
<title> - Acuse Recibo -</title>
</head>
<body>
<center>
<h1>Acuse de Recibo</h1>

<form action="../logica/acuse_intercambio_RecepcionDTE.php" method="POST">

<table>
<tr>
<td>Nombre de Archivo</td><td><input type="text" id="filename" name="filename" size="80" value="dte_76293285-7_34_51.xml"></td>
</tr>
<tr>
<td>Rut Receptor</td><td><input type="text" id="rutReceptor" name="rutReceptor" size="40" value="6673365-3" ></td>
</tr>
<tr>
<td>Rut Emisor</td><td><input type="text" id="rutEmisor" name="rutEmisor" size="40" value="76293285-7" ></td>
</tr>
<tr>
<td>Estado Documento</td>
<td>
<select name="estadoDocumento" id="estadoDocumento">
<option value="0">DTE Recibido OK </option>
<option value="1">DTE No Recibido - Error de Firma </option>
<option value="2">DTE No Recibido - Error en RUT Emisor </option>
<option value="3">DTE No Recibido - Error en RUT Receptor </option>
<option value="4">DTE No Recibido - DTE Repetido </option>
<option value="99">DTE No Recibido - Otros </option>
</select>
</td>
</tr>
<tr>
<td>Estado Envio</td>
<td>
<select name="estadoEnvio" id="estadoEnvio">
<option value="0">Envio Recibido Conforme </option>
<option value="1">Envio Rechazado - Error de Schema </option>
<option value="2">Envio Rechazado - Error de Firma </option>
<option value="3">Envio Rechazado - RUT Receptor No Corresponde </option>
<option value="90">Envio Rechazado - Archivo Repetido </option>
<option value="91">Envio Rechazado - Archivo Ilegible </option>
<option value="99">Envio Rechazado - Otros </option>
</select>
</td>
</tr>
<tr>
<td>&nbsp;</td><td><input type="submit" value="Generar"></td>
</tr>
</form>
</center>
</body>
</html>