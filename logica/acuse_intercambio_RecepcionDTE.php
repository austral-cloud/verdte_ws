<?php

header('Content-type: text/plain; charset=ISO-8859-1');

include 'inc.php';

//---------------------------------------------------------------------------------------------------------------------------------------------
//PARAMETROS A COMPLETAR
$archivo_recibido = "../interfaz/".$_POST['filename']; 
$RutReceptor_esperado = $_POST['rutReceptor']; 
$RutEmisor_esperado = $_POST['rutEmisor']; 


$IdEstadoDocumento = $_POST['estadoDocumento']; 
$StrEstadoDocumento = "";
if($IdEstadoDocumento == 0) { $StrEstadoDocumento = "DTE Recibido OK"; }
if($IdEstadoDocumento == 1) { $StrEstadoDocumento = "DTE No Recibido - Error de Firma"; }
if($IdEstadoDocumento == 2) { $StrEstadoDocumento = "DTE No Recibido - Error en RUT Emisor"; }
if($IdEstadoDocumento == 3) { $StrEstadoDocumento = "DTE No Recibido - Error en RUT Receptor"; }
if($IdEstadoDocumento == 4) { $StrEstadoDocumento = "DTE No Recibido - DTE Repetido"; }
if($IdEstadoDocumento == 99) { $StrEstadoDocumento = "DTE No Recibido - Otros"; }

$IdEstadoEnvio = $_POST['estadoEnvio']; 
$StrEstadoEnvio = "";
if ($IdEstadoEnvio == 0) { $StrEstadoEnvio = "Envío Recibido Conforme"; }
if ($IdEstadoEnvio == 1) { $StrEstadoEnvio = "Envío Rechazado - Error de Schema"; }
if ($IdEstadoEnvio == 2) { $StrEstadoEnvio = "Envío Rechazado - Error de Firma"; }
if ($IdEstadoEnvio == 3) { $StrEstadoEnvio = "Envío Rechazado - RUT Receptor No Corresponde"; }
if ($IdEstadoEnvio == 90) { $StrEstadoEnvio = "Envío Rechazado - Archivo Repetido"; }
if ($IdEstadoEnvio == 91) { $StrEstadoEnvio = "Envío Rechazado - Archivo Ilegible"; }
if ($IdEstadoEnvio == 99) { $StrEstadoEnvio = "Envío Rechazado - Otros"; }

//---------------------------------------------------------------------------------------------------------------------------------------------
// Cargar EnvioDTE y extraer arreglo con datos de carátula y DTEs
$EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
$EnvioDte->loadXML(file_get_contents($archivo_recibido));
$Caratula = $EnvioDte->getCaratula();
$Documentos = $EnvioDte->getDocumentos();

$caratula = [
    'RutResponde' => $RutReceptor_esperado,
    'RutRecibe' => $Caratula['RutEmisor'],
    'IdRespuesta' => 1,
];

// procesar cada DTE
$RecepcionDTE = [];
foreach ($Documentos as $DTE) {
    $estado = $DTE->getEstadoValidacion(['RUTEmisor'=>$RutEmisor_esperado, 'RUTRecep'=>$RutReceptor_esperado]);
    $RecepcionDTE[] = [
        'TipoDTE' => $DTE->getTipo(),
        'Folio' => $DTE->getFolio(),
        'FchEmis' => $DTE->getFechaEmision(),
        'RUTEmisor' => $DTE->getEmisor(),
        'RUTRecep' => $DTE->getReceptor(),
        'MntTotal' => $DTE->getMontoTotal(),
        'EstadoRecepDTE' => $IdEstadoDocumento,
        'RecepDTEGlosa' => $StrEstadoDocumento,
    ];
}

$RespuestaEnvio = new \sasco\LibreDTE\Sii\RespuestaEnvio();
$RespuestaEnvio->agregarRespuestaEnvio([
    'NmbEnvio' => basename($archivo_recibido),
    'CodEnvio' => 1,
    'EnvioDTEID' => $EnvioDte->getID(),
    'Digest' => $EnvioDte->getDigest(),
    'RutEmisor' => $EnvioDte->getEmisor(),
    'RutReceptor' => $DTE->getReceptor(), 
    'EstadoRecepEnv' => $IdEstadoEnvio,
    'RecepEnvGlosa' => $StrEstadoEnvio,
    'NroDTE' => count($RecepcionDTE),
    'RecepcionDTE' => $RecepcionDTE,
]);

$RespuestaEnvio->setCaratula($caratula);
$RespuestaEnvio->setFirma(new \sasco\LibreDTE\FirmaElectronica($config['firma']));


echo $RespuestaEnvio->generar();

?>