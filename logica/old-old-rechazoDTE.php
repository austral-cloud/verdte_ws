<?php

$Ambiente = "2";

$accion= $_POST['accion'];
$estadoDocumento = $_POST['estadoDocumento'];

$RUTEmisor= $_POST['rutEmisor'];
$DV_Emisor = $_POST['dvEmisor'];
$TipoDTE  = $_POST['tipoDocumento'];
$Folio = $_POST['folio'];

//---------------------------------------------------------------------

// respuesta en texto plano
header('Content-type: text/html; charset=utf-8');

include 'inc.php';

$Url = "";
if($Ambiente == "1")
{
//include 'incCertificacion.php';
$Url = "https://ws1.sii.cl/WSREGISTRORECLAMODTECERT/registroreclamodteservice?wsdl";
}
else
{
//include 'incProduccion.php';
$Url = "https://ws1.sii.cl/WSREGISTRORECLAMODTE/registroreclamodteservice?wsdl";
}

//error_reporting(0);

// solicitar token
$token = \sasco\LibreDTE\Sii\Autenticacion::getToken($config['firma']);
if (!$token) {
    foreach (\sasco\LibreDTE\Log::readAll() as $error)
        echo $error,"\n";
    exit;
}

//echo $token;
//echo "antes de soap";
//echo "<br>";
//echo $Url;

try
{
  ini_set("soap.wsdl_cache_enabled", "0");
  
  //respaldo
  //$client = new SoapClient($Url, array('trace' => true));
  
  $client = new SoapClient($Url);
  $client->__setCookie("TOKEN", $token);
  
  
  
  //  echo "accion : ".$accion;
  
  if($accion == "2")
  {
    var_dump($client->listarEventosHistDoc($RUTEmisor, $DV_Emisor, $TipoDTE, $Folio));
  
    //echo $client->__getLastResponse();  
    /*
    $some_struct = $client->listarEventosHistDoc($RUTEmisor, $DV_Emisor, $TipoDTE, $Folio);
  
    echo $some_struct->codResp; 
    echo "~";
    echo $some_struct->descResp; 
    //echo "~";
    //var_dump($some_struct);
    //$array = $some_struct->listaEventosDoc;
    */
    
  }

    if($accion == "2.2")
    {
        $event = $client->listarEventosHistDoc($RUTEmisor, $DV_Emisor, $TipoDTE, $Folio);

        header('Content-Type: application/json');

        $data = [
            'codResp' => $event->codResp,
            'descResp' => $event->descResp
        ];

        if(isset($event->listaEventosDoc))
        {
            $data['listaEventosDoc'] = [
                'codEvento' => $event->listaEventosDoc->codEvento,
                'descEvento' => $event->listaEventosDoc->descEvento,
                'rutResponsable' => $event->listaEventosDoc->rutResponsable,
                'dvResponsable' => $event->listaEventosDoc->dvResponsable,
                'fechaEvento' => $event->listaEventosDoc->fechaEvento
            ];
        }

        echo json_encode($data);
    }
  
  if($accion == "3")
  {
    $some_struct = $client->consultarDocDteCedible($RUTEmisor, $DV_Emisor, $TipoDTE, $Folio);
  
    echo $some_struct->codResp; 
    echo "~";
    echo $some_struct->descResp;
    //echo "~";
    //var_dump($some_struct);
    //$array = $some_struct->listaEventosDoc;
  }
  
  if($accion == "4")
  {  
    $fechaRecepcion =  $client->consultarFechaRecepcionSii($RUTEmisor, $DV_Emisor, $TipoDTE, $Folio);
    echo $fechaRecepcion;   
  }
  
  
  if($accion == "1")
  {
    $some_struct = $client->ingresarAceptacionReclamoDoc($RUTEmisor, $DV_Emisor, $TipoDTE, $Folio, $estadoDocumento);
    echo $some_struct->codResp;
    echo "~";
    echo $some_struct->descResp;
  }

    if($accion == "1.1")
    {
        $some_struct = $client->ingresarAceptacionReclamoDoc($RUTEmisor, $DV_Emisor, $TipoDTE, $Folio, $estadoDocumento);

        header('Content-Type: application/json');

        echo json_encode([
            'codResp' => $some_struct->codResp,
            'descResp' => $some_struct->descResp
        ]);
    }
  
  
  /*
  $some_struct = $client->respuestaTo();
  $some_struct->codResp = "0";
  $some_struct->descResp = "asas";
  echo $some_struct;
  */
  
  //$array = (array)$RespuestaDTE;
  //echo $array;  
  //respuestaTo
}
catch (Exception $ex)   
{
  echo $ex->getMessage();
  echo "Error Codigo: ".$ex->faultcode;
  echo "~";
  echo "Error Detalle: ".$ex->faultstring;
}


/*
// consultar estado dte
$xml = \sasco\LibreDTE\Sii::request('QueryEstDte', 'getEstDte', [
    'RutConsultante'    => '15966258',
    'DvConsultante'     => '6',
    'RutCompania'       => '76293285',
    'DvCompania'        => '7',
    'RutReceptor'       => $RUTReceptor,
    'DvReceptor'        => $RUTRecep_dv,
    'TipoDte'           => $TipoDTE,
    'FolioDte'          => $Folio,
    'FechaEmisionDte'   => $FechaEmisionDte,
    'MontoDte'          => $MntDte,
    'token'             => $token,
]);
//'FechaEmisionDte'   => '2016-07-08',
// FechaEmisionDte => '08072016'

$array = (array)$xml->xpath('/SII:RESPUESTA/SII:RESP_HDR')[0];

$EstadoDte = $array['ESTADO'];
$glosaestadoDte  = $array['GLOSA_ESTADO'];

$codeError = $array['ERR_CODE'];
$desError = $array['GLOSA_ERR'];

$fecharesp = $array['NUM_ATENCION'];


*/


?>