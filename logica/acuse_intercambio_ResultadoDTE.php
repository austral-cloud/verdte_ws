<?php

header('Content-type: text/plain; charset=ISO-8859-1');

include 'inc.php';

//-----------------------------------------------------------------------------------------------------------------------------------------
//DATOS PARA COMPLETAR

$archivo_recibido = "../interfaz/".$_POST['filename']; 
$RutReceptor_esperado = $_POST['rutReceptor']; 
$RutEmisor_esperado = $_POST['rutEmisor']; 

$idEstado = $_POST['estadoDocumento']; 
$strEstado = "";

if($idEstado == 0)	{	$strEstado = "ACEPTADO OK";	}

if($idEstado == 1)	{	$strEstado = "ACEPTADO CON DISCREPANCIAS";	}

if($idEstado == 2)	{	$strEstado = "RECHAZADO";	}

//-----------------------------------------------------------------------------------------------------------------------------------------

$EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
$EnvioDte->loadXML(file_get_contents($archivo_recibido));
$Caratula = $EnvioDte->getCaratula();
$Documentos = $EnvioDte->getDocumentos();

$caratula = [
    'RutResponde' => $RutReceptor_esperado,
    'RutRecibe' => $Caratula['RutEmisor'],
    'IdRespuesta' => 1,
];

$RespuestaEnvio = new \sasco\LibreDTE\Sii\RespuestaEnvio();

foreach ($Documentos as $DTE) 
{
    $RespuestaEnvio->agregarRespuestaDocumento([
        'TipoDTE' => $DTE->getTipo(),
        'Folio' => $DTE->getFolio(),
        'FchEmis' => $DTE->getFechaEmision(),
        'RUTEmisor' => $DTE->getEmisor(),
        'RUTRecep' => $DTE->getReceptor(),
        'MntTotal' => $DTE->getMontoTotal(),
        'CodEnvio' => 1,
        'EstadoDTE' => $idEstado,
        'EstadoDTEGlosa' => $strEstado,
    ]);
}

$RespuestaEnvio->setCaratula($caratula);
$RespuestaEnvio->setFirma(new \sasco\LibreDTE\FirmaElectronica($config['firma']));

echo $RespuestaEnvio->generar();

?>