<?php

header('Content-type: text/plain; charset=ISO-8859-1');

include 'inc.php';

//-------------------------------------------------------------------------------------------------------------------------------------------------
// datos
$archivo_recibido = "../interfaz/".$_POST['filename'];
$RutResponde = $_POST['rutEmisor']; 
$RutFirma = $_POST['rutContacto']; 
$Recinto = $_POST['recinto'];
$NmbContacto = $_POST['nombreContacto']; 
$MailContacto = $_POST['mailContacto'];

//-------------------------------------------------------------------------------------------------------------------------------------------------

$EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
$EnvioDte->loadXML(file_get_contents($archivo_recibido));
$Caratula = $EnvioDte->getCaratula();
$Documentos = $EnvioDte->getDocumentos();

$caratula = [
    'RutResponde' => $RutResponde,
    'RutRecibe' => $Caratula['RutEmisor'],
    'NmbContacto' => $NmbContacto,
    'MailContacto' => $MailContacto,
];

$EnvioRecibos = new \sasco\LibreDTE\Sii\EnvioRecibos();
$EnvioRecibos->setCaratula($caratula);
$EnvioRecibos->setFirma(new \sasco\LibreDTE\FirmaElectronica($config['firma']));

foreach ($Documentos as $DTE) {
    $EnvioRecibos->agregar([
        'TipoDoc' => $DTE->getTipo(),
        'Folio' => $DTE->getFolio(),
        'FchEmis' => $DTE->getFechaEmision(),
        'RUTEmisor' => $DTE->getEmisor(),
        'RUTRecep' => $DTE->getReceptor(),
        'MntTotal' => $DTE->getMontoTotal(),
        'Recinto' => $Recinto,
        'RutFirma' => $RutFirma,
    ]);
}
echo $EnvioRecibos->generar();

?>